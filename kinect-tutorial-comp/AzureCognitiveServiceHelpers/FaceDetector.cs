﻿using Microsoft.ProjectOxford.Face;
using Microsoft.ProjectOxford.Face.Contract;
using System;
using System.IO;

namespace PeterHo.AzureCognitiveServiceHelpers {

    class FaceDetector {

        public delegate void OnDetectionComplete(Face[] faces);
        public delegate void OnDetectionFailed(Exception error);

        // The string inside is the Cognitive Service Face API Key generated in Microsoft Azure
        // This key is used for educational purpose only and it may invalided sooner or later
        // Please go to Azure Cognitive Service website and register one free key for yourself!
        // Note that this key is a free key which only allows 20 calls per minute!
        private readonly IFaceServiceClient faceServiceClient = new FaceServiceClient("48a64bf9fccf47deb7be181a9afbd0ac");

        // Singleton design pattern, make sure the whole program only exist one FaceDector instance
        private static FaceDetector singletonInstance = null;
        private FaceDetector() { }

        // Singleton design pattern: method to get the unique instance
        public static FaceDetector getInstance() {
            if(FaceDetector.singletonInstance == null) {
                FaceDetector.singletonInstance = new FaceDetector();
            }
            return FaceDetector.singletonInstance;
        }

        public async void DetectFace(Stream imageStream, OnDetectionComplete completeCallback, OnDetectionFailed failCallback) {
            try {
                // Ask the cognitive API to return all face attributes and data
                Face[] faces = await faceServiceClient.DetectAsync(imageStream, true, true, new FaceAttributeType[] {
                    FaceAttributeType.Age,
                    FaceAttributeType.Gender,
                    FaceAttributeType.Smile,
                    FaceAttributeType.FacialHair,
                    FaceAttributeType.HeadPose,
                    FaceAttributeType.Glasses,
                });

                if (completeCallback != null) {
                    completeCallback(faces); // Finished, return the result via the callback function
                }
            } catch (Exception error) {
                if(failCallback != null) {
                    failCallback(error); // Error, inform the callee via callback function
                }
            }
        }

    }  // End FaceDetector class

} // End namespace

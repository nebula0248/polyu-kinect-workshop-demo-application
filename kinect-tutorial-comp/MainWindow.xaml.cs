﻿using System.Windows;
using System.ComponentModel;
using System.Windows.Media.Imaging;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Windows.Threading;
using Microsoft.Kinect;
using Microsoft.Kinect.Wpf.Controls;
using Microsoft.ProjectOxford.Face.Contract;
using PeterHo.KinectExtensions;
using PeterHo.AzureCognitiveServiceHelpers;

namespace kinect_tutorial_comp {

    public partial class MainWindow : Window {

        private KinectSensor kinectSensor = null;
        private UserDetectionController userDetectCtrl = null;
        private SpeechRecognitionController speechRecoCtrl = null;
        private CameraImageCapturer camImgCapturer = null;
        private SwipeLeftGestureDetector swipeGestDetector = null;

        // Use a list to store the product item information
        private List<string[]> productList = null;

        // Use a timer to countdown before analysing face
        DispatcherTimer faceAnalysisTimer = null;

        public MainWindow() {
            this.InitializeComponent();  // Initialize all UI elements of the window form
            this.InitializeProductLists();

            // Hide those price and product label when the app is started at the very beginning
            priceLabel.Visibility = Visibility.Hidden;
            productLabel.Visibility = Visibility.Hidden;
        }

        /*
         * 
         *  Produce name and price list management
         * 
         */
        private void InitializeProductLists() {
            this.productList = new List<string[]>();

            // The value is an string array in the format of [image-path, produce-name, price-string]
            this.productList.Add(new string[] {
                "Resources/chip.jpg",
                "Potato chip",
                "$12"
            });
            this.productList.Add(new string[] {
                "Resources/coke.jpg",
                "Coca-cola",
                "$20"
            });
            this.productList.Add(new string[] {
                "Resources/cup-noodle.jpg",
                "Cup noodle",
                "$40"
            });
        }

        /*
         * 
         *  Window WFP form events
         *  
         */
        private void Window_Loaded(object sender, RoutedEventArgs e) {
            this.kinectSensor = KinectSensor.GetDefault();
            this.kinectSensor.Open();

            // Part 1: Initialize a controller to detect speech
            this.speechRecoCtrl = new SpeechRecognitionController();
            this.speechRecoCtrl.Start(this.kinectSensor,
                Properties.Resources.SpeechGrammar, OnGrammarSpeechDetected);

            // Part 2: Initialize a KinectRegion to enable a Kinect hand-controlled cursor
            KinectRegion.SetKinectRegion(this, rootKinectRegion);
            this.rootKinectRegion.KinectSensor = this.kinectSensor;

            // Part 3: Initialize a controller to detect user enter/exit 
            // and a detector to monitor the swipe gesture from the user
            this.userDetectCtrl = new UserDetectionController();
            this.userDetectCtrl.Start(this.kinectSensor, OnUserAmountChanged);
            this.swipeGestDetector = new SwipeLeftGestureDetector();
            this.swipeGestDetector.Start(kinectSensor, OnSwipeGestureDetected);

            // Addition: Initialize to capture the latest camera image
            this.camImgCapturer = new CameraImageCapturer();
            this.camImgCapturer.Start(kinectSensor);
        }

        private void Window_Closing(object sender, CancelEventArgs e) {
            // Remember to release memories and used objects, which is a good practice
            this.userDetectCtrl.Stop();
            this.speechRecoCtrl.Stop();
            this.camImgCapturer.Stop();
            this.swipeGestDetector.Stop();

            if (this.kinectSensor != null) {
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }
        }

        private void NextProductButton_Click(object sender, RoutedEventArgs e) {
            this.ShowRandomProduct();
        }

        /*
         * 
         *  Kinect event handlers 
         * 
         */
        private void OnGrammarSpeechDetected(string sementicWordDetected) {
            switch (sementicWordDetected) {
                case "CHIP":
                    this.ShowProduct(0);
                    break;
                case "COKE":
                    this.ShowProduct(1);
                    break;
                case "CUP_NOODLE":
                    this.ShowProduct(2);
                    break;
            }
        }

        private void OnUserAmountChanged(int currentUserAmount) {
            // Inform the swipe detector about the latest user body tracking id
            this.swipeGestDetector.SetBodyTrackingId(this.userDetectCtrl.GetUserBodyTrackingId());

            // When a user is detected, he/she may not facing the camera correctly at that instant time
            // we need to give these users a little amount of time to make sure they are facing the
            // camera for us to capture image
            if (currentUserAmount > 0) {
                if (this.faceAnalysisTimer == null) {
                    this.faceAnalysisTimer = new DispatcherTimer();
                    this.faceAnalysisTimer.Interval = new TimeSpan(0, 0, 0, 2, 0); // Wait for 2 seconds

                    // When the timer has counted to 2 seconds, the FaceAnalysisTimer_Tick function will be called
                    this.faceAnalysisTimer.Tick += new EventHandler(FaceAnalysisTimer_Tick);
                }

                this.faceAnalysisTimer.Stop(); // Make sure there is no timer running
                this.faceAnalysisTimer.Start();
            }
        }

        private void OnSwipeGestureDetected() {
            this.ShowRandomProduct();
        }

        /*
         * 
         *  Face analysing functions
         * 
         */
        private void FaceAnalysisTimer_Tick(object sender, EventArgs e) {
            // We just need to analysis once so stop the timer to loop
            this.faceAnalysisTimer.Stop();

            // Start to upload the image captured to the Azure Face API cognitive service
            FaceDetector.getInstance().DetectFace(this.camImgCapturer.GetCapturedCameraImageStream(),
                delegate(Face[] faces) { // Detection success callback
                    if(faces.Length > 0) {
                        this.OnUserFaceAnalysed(faces[0]);
                    }
                },
                delegate(Exception error) { // Detection fail callback
                    MessageBox.Show("Unable to retrieve Face data from Microsoft Cognitive Face API Service!", 
                        "Face analysis failed", MessageBoxButton.OK, MessageBoxImage.Error);
                });
        }

        private void OnUserFaceAnalysed(Face theUserFace) {
            // Feel free to modify the logic to suit your needs,
            // make use of the face object's data and play around!

            if(theUserFace.FaceAttributes.Gender.ToLower().Equals("male")) {
                if(theUserFace.FaceAttributes.Glasses == Glasses.ReadingGlasses) {
                    this.ShowProduct(0);
                } else if(theUserFace.FaceAttributes.Age < 30) {
                    this.ShowProduct(1);
                }
            }
        }

        /*
         * 
         *  Helper functions
         * 
         */
        private void ShowRandomProduct() {
            this.ShowProduct(new Random().Next(0, this.productList.Count));
        }

        private void ShowProduct(int productID) {
            this.DisplayProductImage(this.productList[productID][0]);
            this.DisplayPriceTag(this.productList[productID][1], this.productList[productID][2]);
        }

        private void DisplayProductImage(string imagePath) {
            displayImage.Source = new BitmapImage(new Uri(imagePath, UriKind.Relative));
            displayImage.Visibility = Visibility.Visible;
        }

        private void DisplayPriceTag(string productName, string price) {
            productLabel.Content = productName;
            priceLabel.Content = price;
            productLabel.Visibility = Visibility.Visible;
            priceLabel.Visibility = Visibility.Visible;
        }

    } // End of MainWindow class

} // End of namespace

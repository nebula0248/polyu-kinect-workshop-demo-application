﻿using Microsoft.Kinect;
using Microsoft.Speech.AudioFormat;
using Microsoft.Speech.Recognition;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace PeterHo.KinectExtensions {

    class SpeechRecognitionController {

        private KinectAudioStream convertStream = null;
        private SpeechRecognitionEngine speechEngine = null;

        public delegate void OnGrammarSpeechDetected(string sementicWordDetected);
        private OnGrammarSpeechDetected callbackSaved = null;

        public void Start(KinectSensor kinectSensor, string grammarFileContent, OnGrammarSpeechDetected callback) {
            // We need to capture the speech from the Kinect Audio stream,
            // but before that, we need to conver the 32-bit audio stream into 16-bit audio stream
            IReadOnlyList<AudioBeam> audioBeamList = kinectSensor.AudioSource.AudioBeams;
            Stream audioStream = audioBeamList[0].OpenInputStream();
            this.convertStream = new KinectAudioStream(audioStream);

            // Enable the recognition function
            RecognizerInfo ri = this.TryGetKinectRecognizer();
            if (null != ri) {
                this.speechEngine = new SpeechRecognitionEngine(ri.Id);

                using (var memoryStream = new MemoryStream(Encoding.ASCII.GetBytes(grammarFileContent))) {
                    var g = new Grammar(memoryStream);
                    this.speechEngine.LoadGrammar(g);
                }

                // Register event handler
                this.callbackSaved = callback;
                this.speechEngine.SpeechRecognized += SpeechEngineRecognizedSpeech;

                // Activate the speech stream to start listening to the audio stream
                this.convertStream.SpeechActive = true;
                this.speechEngine.SetInputToAudioStream(
                    this.convertStream,
                    new SpeechAudioFormatInfo(EncodingFormat.Pcm, 16000, 16, 1, 32000, 2, null)
                );
                this.speechEngine.RecognizeAsync(RecognizeMode.Multiple);
            }
        }

        public void Stop() {
            if(this.convertStream != null) {
                this.convertStream.SpeechActive = false;
                this.convertStream = null;
            }

            if(this.speechEngine != null) {
                this.speechEngine.SpeechRecognized -= SpeechEngineRecognizedSpeech;
                this.speechEngine.RecognizeAsyncStop();
                this.speechEngine = null;
            }

            if(this.callbackSaved != null) {
                this.callbackSaved = null;
            }
        }

        private void SpeechEngineRecognizedSpeech(object sender, SpeechRecognizedEventArgs e) {
            if(callbackSaved != null) {
                // Speech utterance confidence below which we treat speech as if it hadn't been heard
                // The confidence value is between 0 to 1
                const double ConfidenceThreshold = 0.3;

                if (e.Result.Confidence >= ConfidenceThreshold) {
                    this.callbackSaved(e.Result.Semantics.Value.ToString());
                }
            }
        }

        private RecognizerInfo TryGetKinectRecognizer() {
            IEnumerable<RecognizerInfo> recognizers;

            // This is required to catch the case when an expected recognizer is not installed.
            // By default - the x86 Speech Runtime is always expected. 
            try {
                recognizers = SpeechRecognitionEngine.InstalledRecognizers();
            } catch (COMException) {
                return null;
            }

            foreach (RecognizerInfo recognizer in recognizers) {
                string value;
                recognizer.AdditionalInfo.TryGetValue("Kinect", out value);
                if ("True".Equals(value, StringComparison.OrdinalIgnoreCase) && "en-US".Equals(recognizer.Culture.Name, StringComparison.OrdinalIgnoreCase)) {
                    return recognizer;
                }
            }

            return null;
        }

        // Inner class KinectAudioStream
        class KinectAudioStream : Stream {

            public bool SpeechActive { get; set; }

            public override bool CanRead {
                get { return true; }
            }

            public override bool CanWrite {
                get { return false; }
            }

            public override bool CanSeek {
                get { return false; }
            }

            public override long Position {
                get { return 0; }
                set { throw new NotImplementedException(); }
            }

            public override long Length {
                get { throw new NotImplementedException(); }
            }

            private Stream kinect32BitStream;

            // Constructor
            public KinectAudioStream(Stream input) {
                this.kinect32BitStream = input;
            }

            public override void Flush() {
                throw new NotImplementedException();
            }

            public override long Seek(long offset, SeekOrigin origin) {
                return 0;
            }

            public override void SetLength(long value) {
                throw new NotImplementedException();
            }

            public override void Write(byte[] buffer, int offset, int count) {
                throw new NotImplementedException();
            }

            public override int Read(byte[] buffer, int offset, int count) {
                // Kinect gives 32-bit float samples. Speech asks for 16-bit integer samples.
                const int SampleSizeRatio = sizeof(float) / sizeof(short); // = 2. 

                // Speech reads at high frequency - allow some wait period between reads (in msec)
                const int SleepDuration = 50;

                // Allocate buffer for receiving 32-bit float from Kinect
                int readcount = count * SampleSizeRatio;
                byte[] kinectBuffer = new byte[readcount];

                int bytesremaining = readcount;

                // Speech expects all requested bytes to be returned
                while (bytesremaining > 0) {
                    // If we are no longer processing speech commands, exit
                    if (!this.SpeechActive) {
                        return 0;
                    }

                    int result = this.kinect32BitStream.Read(kinectBuffer, readcount - bytesremaining, bytesremaining);
                    bytesremaining -= result;

                    // Speech will read faster than realtime - wait for more data to arrive
                    if (bytesremaining > 0) {
                        System.Threading.Thread.Sleep(SleepDuration);
                    }
                }

                // Convert each float audio sample to short
                for (int i = 0; i < count / sizeof(short); i++) {
                    // Extract a single 32-bit IEEE value from the byte array
                    float sample = BitConverter.ToSingle(kinectBuffer, i * sizeof(float));

                    // Make sure it is in the range [-1, +1]
                    if (sample > 1.0f) {
                        sample = 1.0f;
                    } else if (sample < -1.0f) {
                        sample = -1.0f;
                    }

                    // Scale float to the range (short.MinValue, short.MaxValue] and then 
                    // convert to 16-bit signed with proper rounding
                    short convertedSample = Convert.ToInt16(sample * short.MaxValue);

                    // Place the resulting 16-bit sample in the output byte array
                    byte[] local = BitConverter.GetBytes(convertedSample);
                    Buffer.BlockCopy(local, 0, buffer, offset + (i * sizeof(short)), sizeof(short));
                }

                return count;
            } // End Read() function

        } // End KinectAudioStream inner class

    } // End SpeechRecognitionController class

} // End namespace

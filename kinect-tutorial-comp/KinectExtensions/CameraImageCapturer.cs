﻿using Microsoft.Kinect;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PeterHo.KinectExtensions {

    class CameraImageCapturer {

        private ColorFrameReader colorFrameReader = null;
        private WriteableBitmap colorBitmap = null;

        public void Start(KinectSensor kinectSensor) {
            this.colorFrameReader = kinectSensor.ColorFrameSource.OpenReader();

            FrameDescription colorFrameDescription = kinectSensor
                .ColorFrameSource.CreateFrameDescription(ColorImageFormat.Bgra);
            this.colorBitmap = new WriteableBitmap(colorFrameDescription.Width, 
                colorFrameDescription.Height, 96.0, 96.0, PixelFormats.Bgr32, null);

            // Register color frame event handler
            this.colorFrameReader.FrameArrived += this.Reader_ColorFrameArrived;
        }

        public void Stop() {
            if(this.colorFrameReader != null) {
                this.colorFrameReader.Dispose();
                this.colorFrameReader = null;
            }

            if(this.colorBitmap != null) {
                this.colorBitmap = null;
            }
        }

        public MemoryStream GetCapturedCameraImageStream() {
            MemoryStream stream = new MemoryStream();
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();

            encoder.Frames.Add(BitmapFrame.Create(this.colorBitmap));
            encoder.Save(stream);
            stream.Position = 0;

            return stream;
        }

        public BitmapImage GetCapturedCameraImage() {
            BitmapImage bitmap = new BitmapImage();

            using (MemoryStream stream = this.GetCapturedCameraImageStream()) {
                bitmap.BeginInit();
                bitmap.StreamSource = stream;
                bitmap.CacheOption = BitmapCacheOption.OnLoad;
                bitmap.EndInit();
                bitmap.Freeze();
            }

            return bitmap;
        }

        private void Reader_ColorFrameArrived(object sender, ColorFrameArrivedEventArgs e) {
            using (ColorFrame colorFrame = e.FrameReference.AcquireFrame()) {
                if (colorFrame != null) {
                    FrameDescription colorFrameDescription = colorFrame.FrameDescription;

                    using (KinectBuffer colorBuffer = colorFrame.LockRawImageBuffer()) {
                        this.colorBitmap.Lock();

                        // Verify data and write the new color frame data to the display bitmap
                        if ((colorFrameDescription.Width == this.colorBitmap.PixelWidth) && (colorFrameDescription.Height == this.colorBitmap.PixelHeight)) {
                            colorFrame.CopyConvertedFrameDataToIntPtr(
                                this.colorBitmap.BackBuffer,
                                (uint)(colorFrameDescription.Width * colorFrameDescription.Height * 4),
                                ColorImageFormat.Bgra);

                            this.colorBitmap.AddDirtyRect(new Int32Rect(0, 0, 
                                this.colorBitmap.PixelWidth, this.colorBitmap.PixelHeight));
                        }

                        this.colorBitmap.Unlock();
                    }
                }
            }
        } // End Reader_ColorFrameArrived() function

    } // End class CameraImageCapturer

} // End namespace
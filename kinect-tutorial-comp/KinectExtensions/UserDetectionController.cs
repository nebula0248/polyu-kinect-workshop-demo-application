﻿using Microsoft.Kinect;

namespace PeterHo.KinectExtensions {

    class UserDetectionController {

        private BodyFrameReader bodyFrameReader = null;
        private Body[] bodies = null;
        private int numOfBodiesLastFrame = 0;

        public delegate void OnUserAmountChanged(int currentUserAmount);
        private OnUserAmountChanged callbackSaved = null;

        public void Start(KinectSensor kinectSensor, OnUserAmountChanged callback) {
            this.bodyFrameReader = kinectSensor.BodyFrameSource.OpenReader();

            // Register a new event handler to handle new body frame
            if (this.bodyFrameReader != null) {
                this.bodyFrameReader.FrameArrived += this.BodyFrameReader_FrameArrived;
            }

            this.callbackSaved = callback;
        }

        public void Stop() {
            if (this.bodyFrameReader != null) {
                this.bodyFrameReader.FrameArrived -= this.BodyFrameReader_FrameArrived;
                this.bodyFrameReader.Dispose();
                this.bodyFrameReader = null;
            }

            if (this.callbackSaved != null) {
                this.callbackSaved = null;
            }
        }

        public ulong? GetUserBodyTrackingId() {
            if (this.bodies != null && this.bodies.Length > 0) {
                foreach(Body body in this.bodies) {
                    if(body.IsTracked) {
                        return body.TrackingId;
                    }
                }
            }
            return null;
        }

        private void BodyFrameReader_FrameArrived(object sender, BodyFrameArrivedEventArgs e) {
            using (BodyFrame bodyFrame = e.FrameReference.AcquireFrame()) {
                if (bodyFrame == null) {
                    return; // No body frame received, do nothing and end the function
                }

                if (this.bodies == null) {
                    // Initialize the array to store the body objects captured by Kinect
                    // bodyFrame.BodyCount = 6 in Kinect v2
                    this.bodies = new Body[bodyFrame.BodyCount];
                }

                // Update the array to store the latest captured body objects
                bodyFrame.GetAndRefreshBodyData(this.bodies);

                // Started to look into the body objects detected and count number of bodies there
                int numOfBodiesCurrentFrame = 0;
                foreach (Body body in bodies) {
                    if (body.IsTracked) {
                        numOfBodiesCurrentFrame++;
                    }
                }

                // Check if there is a change in the number of bodies detected
                if (numOfBodiesCurrentFrame != this.numOfBodiesLastFrame) {
                    this.callbackSaved(numOfBodiesCurrentFrame);
                }

                // Keep the body number to be checked in next body frame
                this.numOfBodiesLastFrame = numOfBodiesCurrentFrame;
            }
        }

    } // End UserDetectionController class

} // End namespace

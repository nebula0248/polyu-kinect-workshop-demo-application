﻿using Microsoft.Kinect;
using Microsoft.Kinect.VisualGestureBuilder;
using System.Collections.Generic;

namespace PeterHo.KinectExtensions {

    class SwipeLeftGestureDetector {

        // Remember to select "Copy always" for the SwipeGesture.gdb in the
        // solution explorer, otherwise you will get a runtime error
        private string gestureDatabase = "Resources/SwipeGesture.gbd"; 
        private string GestureName = "SwipeProgress_Left";
        private VisualGestureBuilderFrameSource vgbFrameSource = null;
        private VisualGestureBuilderFrameReader vgbFrameReader = null;

        // Used to keep track of the progress of the swipe gesture
        enum SwipeLeftGestureState {
            HandOnLeft, HandOnRight
        }
        private SwipeLeftGestureState? state = null;
        private double? timeSecondStartedGesture = null;

        public delegate void OnSwipeGestureDetected();
        private OnSwipeGestureDetected callbackSaved = null;

        public void Start(KinectSensor kinectSensor, OnSwipeGestureDetected callback) {
            this.callbackSaved = callback;

            this.vgbFrameSource = new VisualGestureBuilderFrameSource(kinectSensor, 0);
            this.vgbFrameReader = this.vgbFrameSource.OpenReader();
            this.vgbFrameReader.IsPaused = false;
            this.vgbFrameReader.FrameArrived += this.Reader_GestureFrameArrived;

            using (VisualGestureBuilderDatabase database = new VisualGestureBuilderDatabase(this.gestureDatabase)) {
                foreach (Gesture gesture in database.AvailableGestures) { 
                    if (gesture.Name.Equals(this.GestureName)) {
                        this.vgbFrameSource.AddGesture(gesture);
                    }
                }
            }
        }

        public void Stop() {
            if (this.vgbFrameReader != null) {
                this.vgbFrameReader.FrameArrived -= this.Reader_GestureFrameArrived;
                this.vgbFrameReader.Dispose();
                this.vgbFrameReader = null;
            }

            if (this.vgbFrameSource != null) {
                this.vgbFrameSource.Dispose();
                this.vgbFrameSource = null;
            }
        }

        public void SetBodyTrackingId(ulong? trackingId) {
            if(trackingId != null) {
                this.vgbFrameSource.TrackingId = (ulong)trackingId;
            } else {
                this.vgbFrameSource.TrackingId = 0;
            }
        }

        private void Reader_GestureFrameArrived(object sender, VisualGestureBuilderFrameArrivedEventArgs e) {
            VisualGestureBuilderFrameReference frameReference = e.FrameReference;

            using (VisualGestureBuilderFrame frame = frameReference.AcquireFrame()) {
                if (frame == null) {
                    return; // No frame arrived, bypass
                }

                // Get the gesture results which arrived with the latest frame
                IReadOnlyDictionary<Gesture, ContinuousGestureResult> contGestResults = frame.ContinuousGestureResults;
                if(contGestResults == null) {
                    return; // No continuous frame result returned, bypass
                }

                // Start analysing the gesture data
                foreach (Gesture gesture in this.vgbFrameSource.Gestures) {
                    if (gesture.Name.Equals(this.GestureName) && gesture.GestureType == GestureType.Continuous) {
                        ContinuousGestureResult result = null;
                        contGestResults.TryGetValue(gesture, out result);

                        if (result != null) {
                            // Now the proress result float number is available here
                            // We can make use of the float number to keep track of how the user swipe
                            // These float values are being set in the Visual Gesturer Builder
                            // Remember, values around 0.5 represent the user is not doing the swipe action
                            // values around 0 represent the user is holding his/her right hand (action start)
                            // values around 1 represent the user has moved his/her right hand to the left (swipe finished)

                            if(this.state == null) {
                                if (result.Progress <= 0.25) {
                                    // Right hand raised, swipe action starts
                                    this.state = SwipeLeftGestureState.HandOnRight;
                                    this.timeSecondStartedGesture = frame.RelativeTime.TotalSeconds;
                                }
                            } else {
                                if (frame.RelativeTime.TotalSeconds - this.timeSecondStartedGesture > 2.0) {
                                    // The gesture has been started but it hasn't been finished within 2 seconds
                                    // which means the swipe is too slow! Reset and do not count it as a swipe
                                    this.state = null;
                                    this.timeSecondStartedGesture = null;
                                } else if(result.Progress >= 0.75) {
                                    // Right hand has been moved to the left, swipe action ends
                                    this.callbackSaved();
                                    this.state = null;
                                    this.timeSecondStartedGesture = null;
                                }
                            }
                        } // End result checking
                    }
                } // End foreach
            }
        } // End Reader_GestureFrameArrived() function

    } // End SwipeGestureDetector class

} // End namespace
